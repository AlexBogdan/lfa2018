interface Visitor {
    public void done();
    public void visit(Visitable node);
}
