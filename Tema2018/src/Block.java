import java.util.Stack;

public class Block extends Statement {
    @Override
    public int calculate() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return null;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }
}
