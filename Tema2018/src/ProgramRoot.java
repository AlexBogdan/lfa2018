import java.util.ArrayList;
import java.util.Stack;

public class ProgramRoot implements Expression {

    private ArrayList<Expression> expressions;

    public ProgramRoot() {
        this.expressions = new ArrayList<>();
    }

    public void addExpression(Expression e) {
        this.expressions.add(e);
    }

    @Override
    public int calculate() {
        for (int i = 0; i < expressions.size()-1; i++) {
            expressions.get(i).calculate();
        }
        return expressions.get(expressions.size()-1).calculate();
    }

    @Override
    public String getSymbol() {
        return null;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    public String toString() {
        return "<MainNode>";
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expression e: expressions) {
            e.accept(visitor);
        }
        visitor.done();
    }
}
