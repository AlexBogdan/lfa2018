import java.util.Stack;

public class BExp implements Expression {
    @Override
    public int calculate() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return null;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {

    }
}
