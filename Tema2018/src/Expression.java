import java.util.Stack;

public interface Expression extends Visitable {
    int calculate();
    String getSymbol();
    Stack<Expression> explode();
}
