import java.util.Stack;

public class If extends Statement {
    private BExp cond;
    private Block body1;
    private Block body2;

    public If(BExp cond, Block body1, Block body2) {
        this.cond = cond;
        this.body1 = body1;
        this.body2 = body2;
    }

    @Override
    public int calculate() {
        int res;
        if (this.cond.calculate() == 1) {
            res = this.body1.calculate();
        } else {
            res = this.body2.calculate();
        }
        if (this.next_stmt != null) {
            res = this.next_stmt.calculate();
        }
        return res;
    }

    @Override
    public String getSymbol() {
        String res = "if" + this.cond.getSymbol() + this.body1.getSymbol() + "else" + this.body2.getSymbol();
        if (this.next_stmt != null) {
            res += this.next_stmt.getSymbol();
        } return res;
    }

    public String toString() {
        return "<IfNode> " + "if";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.cond.accept(visitor);
        this.body1.accept(visitor);
        this.body2.accept(visitor);
        if (this.next_stmt != null) {
            this.next_stmt.accept(visitor);
            visitor.done();
        }
        visitor.done();
    }
}
