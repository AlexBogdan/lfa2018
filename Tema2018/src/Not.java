import java.util.Stack;

public class Not extends BExp {
    private BExp e;

    public Not(BExp e) {
        this.e = e;
    }

    public int calculate() {
        return 1 - this.e.calculate();
    }

    public String getSymbol() {
        return "!" + e.getSymbol();
    }

    public String toString() {
        return "<NotNode> " + "!";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.e.accept(visitor);
        visitor.done();
    }
}
