import java.io.BufferedWriter;
import java.io.IOException;

public class ASTVisitor implements Visitor {

    private int tabs;
    private BufferedWriter out;

    public ASTVisitor(BufferedWriter out) {
        this.tabs = 0;
        this.out = out;
    }

    private String getTabs() {
        String result = "";
        for (int i = 0; i < this.tabs; i++) {
          result += "\t";
        }
        return result;
    }

    public void done() {
        this.tabs--;
    }

    @Override
    public void visit(Visitable node) {
        try {
            this.out.write(this.getTabs() + node + "\n");
            this.tabs++;
        }
        catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
    }
}
