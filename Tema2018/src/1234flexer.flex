import java.util.Stack;
import java.util.HashMap;

%%

%class Flexer
%unicode
/*%debug*/
%int
%line
%column
%{

    Boolean exec = false;

    Stack<Expression> s = new Stack<Expression>();
    HashMap<String, Integer> map = new HashMap<>();
    Boolean isBoolean = false;

    void print_stack() {
      Stack<Expression> temp_s = new Stack<Expression>();
      int i = s.size();

      for (int j = 0; j < i; ++j) {
        temp_s.push(s.pop());
      }

      System.out.print("Stiva : ");
      for (int j = 0; j < i; ++j) {
        System.out.print(temp_s.peek().getSymbol() + " ");

        s.push(temp_s.pop());
      }

      System.out.println();
    }

    Expression get_nth_element_from_stack(int i) {
      Stack<Expression> temp_s = new Stack<Expression>();

      if (i > s.size()) {
        return null;
      }

      for (int j = 0; j < i; ++j) {
        temp_s.push(s.pop());
      }

      Expression res = temp_s.peek();

      for (int j = 0; j < i; ++j) {
        s.push(temp_s.pop());
      }

      return res;
    }


    int interpret(String var) {
        return map.get(var);
    }

    void add_var(Expression a) {
    	if (s.isEmpty()) {
   			s.push(a);
   		} else {
        Symbol symb = (Symbol) s.peek();
    		if (symb.getSymbol().equals("+") || symb.getSymbol().equals("/")) {
    			Symbol op = (Symbol) s.pop();
    			if (s.isEmpty()) {
   					//eroare
   				}
    			Expression e = s.pop();
    			Binary b = new Binary(e, a, op);
    			add_var(b);
    		} else {
   				s.push(a);

   			}
   		}
   	}


    void close_par() {
   		if (s.isEmpty()) {
    		//eroare
    	} else {
    	    Atom at = new Atom(s.pop().calculate());
    		Par p = new Par(at);

   			if (s.isEmpty()) {
    			//eroare
    			return;
    		}

    		Symbol symb = (Symbol) s.pop();
    		if (symb.getSymbol().equals("<=")) {
                int valR = p.calculate();
                Boolean res;
                int valL = s.pop().calculate();
                if (valL <= valR) {
                    res = true;
                } else {
                    res = false;
                }
                if(!s.isEmpty()) {
                  // testez daca mai am alt boolean in spate ca sa minimizez
                    if(((Symbol)s.peek()).getSymbol().equals("&&")) {
                       s.pop();
                       if (res && ((BoolAtom) s.pop()).calculate() == 1)
                           res = true;
                       else
                           res = false;
                    }

                  // testez daca am "!" in spate pentru a inversa valoarea
                    if(((Symbol)s.peek()).getSymbol().equals("!")) {
                       s.pop();
                       res = !res;
                    }
                }
                s.push(new BoolAtom(res));

    		} else if(((Symbol)s.peek()).getSymbol().equals("!")) {
           s.pop();
           add_var(new BoolAtom(1 - p.calculate() == 1));
        } else if (symb.getSymbol().equals("(")){
    			add_var(p);
    		}
   		}
   	}

    void assignment() {
      if (s.empty()) {
        // eroare
        return ;
      } else {
        // Calculam valoarea expresiei de pe stiva
        int value = s.pop().calculate();

        if (s.empty()) {
          // eroare
          return ;
        } else {
          // Verificam daca urmeaza un "="
          Symbol symb = (Symbol) s.pop();
          if (symb.getSymbol().equals("=")) {
            if (s.empty()) {
              // eroare
              return ;
            } else {
              // Obtinem numele variabilei si o asociem in HashMap cu valoarea calculata
              String var_name = ( (Symbol) s.pop()).getSymbol();
              map.put(var_name, value);

              System.out.println(var_name + " = " + interpret(var_name));
            }
          }
        }
      }
    }


    void check_if_if() {
      if (get_nth_element_from_stack(3).getSymbol().equals("if")) {
          Expression cond_value = s.peek();
          s.pop();
          s.pop();
          s.pop();
          s.push(new If(cond_value.calculate()));
      }
    }
%}

LineTerminator = \r|\n|\r\n
WS = {LineTerminator} | [ \t\f]

letter = [a-zA-Z]+
digit = [0-9]
special = "_"
var = {letter}({digit}|{letter}|{special})*
number = [1-9]{digit}*
atribuire = {var} "="
boolean = "true"|"false"
block = "{}"
if = "if"
while = "while"
else = "else"

%%

{WS}	{/*Skip whitespace in any state*/}

"+"     { s.push(new Symbol("+")); }
"/"     { s.push(new Symbol("/")); }
{boolean}  {
    s.push(new BoolAtom(yytext()));
    // daca am inainte de asta un && o sa trebuiasca minimizat cu DeMorgan
}
{atribuire} {
    if (exec) {
        String var_name = yytext().split("=")[0];

        s.push(new Symbol(var_name));
        s.push(new Symbol("="));
    }
}
{if} {
    if (exec) {
        System.out.println("Avem un if");
        s.push(new Symbol("if"));
    }
}
{while} {

}
{else}  {
    System.out.println("Avem un ELSE");
}
"{"     {
    System.out.println("Deschidem STMT " + interpret("a"));

    if (get_nth_element_from_stack(2))
 }
"}"     {
    System.out.println("Inchidem STMT");
}
{var}   {
    int nr = interpret(yytext());
    add_var(new Atom(nr));
}
{number}    {
    int nr = Integer.parseInt(yytext());
    add_var(new Atom(nr));
}
"("     {
    s.push(new Symbol("("));

}
")"     {
    close_par();
    check_if_if();
    print_stack();
}
";"     {
    assignment();
}
"<="    {
    s.push(new Symbol("<="));
}
"&&"    {
    close_par();    // se calculeaza valoarea bool inainte de asta
    s.push(new Symbol("&&"));
}
"!"     {
    s.push(new Symbol("!"));
}
