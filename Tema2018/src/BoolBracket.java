import java.util.Stack;

public class BoolBracket extends BExp {
    private BExp b;

    public BoolBracket(BExp b) {
        this.b = b;
    }

    public int calculate() {
        return b.calculate();
    }

    public String getSymbol() {
      return "(" + this.b.getSymbol() + ")";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    public String toString() {
        return "<BracketNode> " + "()";
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.b.accept(visitor);
        visitor.done();
    }
}
