import java.util.Stack;

public class Variable extends AExp {

    private String varName;

    public Variable(String varName) {
        this.varName = varName;
    }

    public String getVarName() {
        return this.varName;
    }

    public int calculate() {
        return Variables.getVarValue(this.varName);
    }

    public String getSymbol() {
        return this.varName;
    }

    public String toString() {
        return "<VariableNode> " + this.varName;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        visitor.done();
    }
}
