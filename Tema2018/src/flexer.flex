import java.util.Stack;
import java.util.HashMap;
import java.util.HashMap;

%%

%class Flexer
%unicode
/*%debug*/
%int
%line
%column
%{
    Stack<Expression> stack = new Stack<>();
    ProgramRoot root = new ProgramRoot();

    void explodeExpression(Expression expr) {
        Stack<Expression> elements = expr.explode();

        while (!elements.empty()) {
            stack.push(elements.pop());
        }
    }

    /**
    *   Afisarea stivei
    */
    void print_stack() {
        Stack<Expression> temp_s = new Stack<>();
        int stack_size = stack.size();

        for (int j = 0; j < stack_size; ++j) {
            temp_s.push(stack.pop());
        }

        System.out.print("Stiva : ");
        for (int j = 0; j < stack_size; ++j) {
            System.out.print(temp_s.peek().getSymbol() + " ");
            stack.push(temp_s.pop());
        }
        System.out.println();
    }


    Expression get_nth_element_from_stack(int element_number) {
        Stack<Expression> temp_stack = new Stack<>();

        if (element_number > stack.size()) {
            return null;
        }

        for (int j = 0; j < element_number; ++j) {
            temp_stack.push(stack.pop());
        }

        Expression res = temp_stack.peek();

        for (int j = 0; j < element_number; ++j) {
            stack.push(temp_stack.pop());
        }
        return res;
    }

    Symbol check_for_symbol() {
        if (!stack.empty() && stack.peek() instanceof Symbol) {
            return (Symbol) stack.peek();
        } else {
            return null;
        }
    }

    Block close_block() {
        Symbol symb = check_for_symbol();
        if (symb != null && symb.getSymbol().equals("{")) {
            stack.pop(); // Scoatem "{" de pe stiva
            return new EmptyBlock();
        } else {
            Expression stmt = stack.pop();
            symb = check_for_symbol();
            if (symb != null && symb.getSymbol().equals("{")) {
                stack.pop(); // Scoatem "{" de pe stiva
                return new StatementBlock((Statement) stmt);
            }
        }
        return null;
    }

    void stack_push(Statement element) {
        if (element != null) {
            if (! stack.empty()) {
                Expression e = stack.peek();
                if (e instanceof Sequence) {
                    ((Sequence) e).addNewStatement(element);
                }
                else if (e instanceof Statement) {
                    Expression new_e = new Sequence((Statement) e, element);
                    stack.pop(); // Scoatem "Statementul" de pe stiva
                    stack_push(new_e);
                } else {
                    stack.push(element);
                }
            } else {
                stack.push(element);
            }
        }
    }

    void stack_push(Block element) {
        if (stack.empty()) {
            stack.push(element);
        } else {
            Expression new_e = element;
            Boolean found = false;
            if (stack.size() >= 1) {
                Expression e1 = get_nth_element_from_stack(1);

                if (e1 instanceof Block || e1 instanceof Sequence || e1 instanceof Statement) {
                    stack_push((Statement) element);
                    return ;
                }
            }
            if (stack.size() >= 2) {
                Expression e1 = get_nth_element_from_stack(1);
                Expression e2 = get_nth_element_from_stack(2);

                if (e2 instanceof Symbol && ((Symbol) e2).getSymbol().equals("while")) {
                    stack.pop(); // Scoatem BExp (e1) de pe Stiva
                    stack.pop(); // Scoatem "while" de pe stiva
                    new_e = new While((BExp) e1, element);
                    found = true;
                }
            }
            if (!found && stack.size() >= 4) {
                Expression e1 = get_nth_element_from_stack(1);
                Expression e2 = get_nth_element_from_stack(2);
                Expression e3 = get_nth_element_from_stack(3);
                Expression e4 = get_nth_element_from_stack(4);

                if (e1 instanceof Symbol && ((Symbol) e1).getSymbol().equals("else")) {
                    stack.pop(); // Scoatem "else" (e1) de pe Stiva
                    stack.pop(); // Scoatem Block (e2) de pe stiva
                    stack.pop(); // Scoatem BExp (e3) de pe Stiva
                    stack.pop(); // Scoatem "if" (e4) de pe stiva
                    new_e = new If((BExp) e3, (Block) e2, element);
                    found = true;
                }
            }
            if (! found) {
                stack.push(element);
                return ;
            }
            stack_push(new_e);
        }
    }

    void stack_push(BExp element) {
        if (stack.empty()) {
            stack.push(element);
        } else {
            Symbol symb = check_for_symbol();
            if (symb != null) {
                Expression new_e = element;
                if (symb.getSymbol().equals("!")) {
                    stack.pop(); // Scoatem "!" de pe sitva
                    new_e = new Not(element);
                }
                else if (symb.getSymbol().equals("&&")) {
                    stack.pop(); // Scoatem "&&" de pe sitva
                    new_e = new And((BExp) stack.pop(), element);
                }
                else {
                    stack.push(new_e);
                    return ;
                }
                stack_push(new_e);
            } else {
                stack.push(element);
            }
        }
    }

    void stack_push(AExp element) {
        if (stack.empty()) {
            stack.push(element);
        } else {
            Symbol symb = check_for_symbol();
            if (symb != null) {
                Expression new_e = element;
                if (symb.getSymbol().equals("+")) {
                    stack.pop(); // Scoatem "+" de pe sitva

                    if (stack.peek() instanceof Greater ||
                        stack.peek() instanceof And) {
                        explodeExpression(stack.pop()); // Spargem Greater in (left, >, right) si le repunem pe stiva
                        stack.push(new Symbol("+"));
                    } else {
                        new_e = new Plus((AExp) stack.pop(), element);
                    }
                }
                else if (symb.getSymbol().equals("/")) {
                    stack.pop(); // Scoatem "+" de pe sitva

                    if (stack.peek() instanceof Greater ||
                        stack.peek() instanceof And ||
                        stack.peek() instanceof Plus) {
                        explodeExpression(stack.pop()); // Spargem Greater in (left, >, right) si le repunem pe stiva
                        stack.push(new Symbol("/"));
                    } else {
                        new_e = new Division((AExp) stack.pop(), element);
                    }

                    /* if (e1 instanceof Plus) {
                        new_e = new Plus(((Plus) e1).getLeft(), new Division(((Plus) e1).getRight(), element));
                    } else {
                        new_e = new Division((AExp) e1, element);
                    } */
                }
                else if (symb.getSymbol().equals(">")) {
                    stack.pop(); // Scoatem ">" de pe sitva
                    new_e = new Greater((AExp) stack.pop(), element);
                }
                else {
                    stack.push(new_e);
                    return ;
                }
                stack_push(new_e);
            } else {
                stack.push(element);
            }
        }
    }

    void stack_push(Expression e) {
        if (e instanceof AExp) {
            stack_push((AExp) e);
        }
        else if (e instanceof BExp) {
            stack_push((BExp) e);
        }
        else if (e instanceof Block) {
            stack_push((Block) e);
        }
        else if (e instanceof Statement) {
            stack_push((Statement) e);
        }
    }

    Expression form_bracket(Expression e) {
        if (e instanceof AExp) {
            return new ArithmeticBracket((AExp) e);
        }
        else if (e instanceof BExp) {
            return new BoolBracket((BExp) e);
        }
        return null;
    }

    void create_var_assignment() {
        if (stack.size() >= 3) {
            Expression e1 = get_nth_element_from_stack(1);
            Expression e2 = get_nth_element_from_stack(2);
            Expression e3 = get_nth_element_from_stack(3);

            if (e2.getSymbol().equals("=")) {
                stack.pop();
                stack.pop();
                stack.pop();
                stack_push(new Assignment((Variable) e3, (AExp) e1));
            }
        }
    }
%}

%eof{
    print_stack();

    Stack<Expression> temp_stack = new Stack<>();

    while (! stack.empty()) {
        temp_stack.push(stack.pop());
    }

    while (! temp_stack.empty()) {
        root.addExpression(temp_stack.pop());
    }

%eof}

LineTerminator = \r|\n|\r\n
WS = {LineTerminator} | [ \t\f]

operator = "+" | "/"
letter = [a-zA-Z]+
digit = [0-9]
special = "_"
var = {letter}({digit}|{letter}|{special})*
number = [1-9]{digit}* | 0

%%

{WS}	{/*Skip whitespace in any state*/}
"int"   {
    stack.push(new Symbol("int"));
    print_stack();
}
","   {}
"+"     {
    stack.push(new Symbol("+"));
    print_stack();
}
"/"     {
    stack.push(new Symbol("/"));
    print_stack();
}
">"    {
    stack.push(new Symbol(">"));
    print_stack();
}
"!"     {
    stack.push(new Symbol("!"));
    print_stack();
}
"&&"    {
    stack.push(new Symbol("&&"));
    print_stack();
}
{number}    {
    stack_push(new Int(Integer.parseInt(yytext())));
    print_stack();
}
"("     {
    stack.push(new Symbol("("));
    print_stack();
}
")"     {
    Expression new_e = form_bracket(stack.pop());
    stack.pop(); // Scoatem "(" de pe stiva
    stack_push(new_e);
    print_stack();
}
"{"     {
    stack.push(new Symbol("{"));
    print_stack();
}
"}"     {
    stack_push(close_block());
}
"="     {
    stack.push(new Symbol("="));
    print_stack();
}
";"     {
    if (! stack.empty() && stack.peek().getSymbol().equals("int")) {
        stack.pop();
    } else{
        create_var_assignment();
        print_stack();
    }
}
"if"    {
    stack.push(new Symbol("if"));
    print_stack();
}
"else"    {
    stack.push(new Symbol("else"));
    print_stack();
}
"while" {
    stack.push(new Symbol("while"));
    print_stack();
}
"true"  {
    stack_push(new Bool(true));
    print_stack();
}
"false" {
     stack_push(new Bool(false));
     print_stack();
}
{var}   {
    if (! stack.empty() && stack.peek().getSymbol().equals("int")) {
        Variables.setVarValue(yytext(), -1);
    } else{
        stack_push(new Variable(yytext()));
        print_stack();
    }
}
