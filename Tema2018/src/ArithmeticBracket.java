import java.util.Stack;

public class ArithmeticBracket extends AExp {
    private AExp a;

    public ArithmeticBracket(AExp a) {
        this.a = a;
    }

    public int calculate() {
        return a.calculate();
    }

    public String getSymbol() {
      return "(" + this.a.getSymbol() + ")";
    }

    public String toString() {
        return "<BracketNode> " + "()";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<Expression>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.a.accept(visitor);
        visitor.done();
    }
}
