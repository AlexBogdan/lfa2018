public interface Visitable {
    public void accept(ASTVisitor visitor);
}
