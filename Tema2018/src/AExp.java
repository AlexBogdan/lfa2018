import java.util.Stack;

public class AExp implements Expression {
    @Override
    public int calculate() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return "";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<Expression>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        
    }
}
