import java.util.Stack;

public class EmptyBlock extends Block {
    public EmptyBlock() {}

    public int calculate() {
        int res = 0;
        if (this.next_stmt != null) {
            res = this.next_stmt.calculate();
        }
        return res;
    }

    public String getSymbol() {
        String res = "{}";
        if (this.next_stmt != null) {
          res += this.next_stmt.getSymbol();
        }
        return res;
    }

    public String toString() {
        return "<BlockNode> " + "{}";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        visitor.done();
        if (this.next_stmt != null) {
            this.next_stmt.accept(visitor);
            visitor.done();
        }
    }
}
