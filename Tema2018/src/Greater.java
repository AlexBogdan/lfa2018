import java.util.Stack;

public class Greater extends BExp {
    private AExp left;
    private AExp right;

    public Greater(AExp left, AExp right) {
        this.left = left;
        this.right = right;
    }

    public int calculate() {
        return (this.left.calculate() > this.right.calculate())? 1 : 0;
    }

    public String getSymbol() {
        return this.left.getSymbol() + ">" + this.right.getSymbol();
    }

    public String toString() {
        return "<GreaterNode> " + ">";
    }

    @Override
    public Stack<Expression> explode() {
        Stack<Expression> stack = new Stack<>();
        stack.push(this.right);
        stack.push(new Symbol(">"));
        stack.push(this.left);

        return stack;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
    }
}
