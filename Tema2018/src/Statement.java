import java.util.Stack;

public class Statement implements Expression {
    protected Statement next_stmt = null;

    public void setNextStatement(Statement stmt) {
        if (this.next_stmt == null) {
            this.next_stmt = stmt;
        } else {
            this.next_stmt.setNextStatement(stmt);
        }
    }

    @Override
    public int calculate() {
        return 0;
    }

    @Override
    public String getSymbol() {
        return null;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {

    }
}
