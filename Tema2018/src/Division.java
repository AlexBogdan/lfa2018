import java.util.Stack;

public class Division extends AExp {
    private AExp left;
    private AExp right;

    public Division(AExp left, AExp right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int calculate() {
        return this.left.calculate() / this.right.calculate();
    }

    @Override
    public String getSymbol() {
      return this.left.getSymbol() + "/" + this.right.getSymbol();
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    public String toString() {
        return "<DivNode> " + "/";
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
    }
}
