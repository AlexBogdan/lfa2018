import java.util.Stack;

public class Sequence extends Statement {
    private Statement left;
    private Statement right;

    public Sequence(Statement left, Statement right) {
        this.left = left;
        this.right = right;
    }

    public Statement getLeft() {
        return this.left;
    }

    public Statement getRight() {
        return  this.right;
    }

    public void addNewStatement(Statement stmt) {
        if (this.right instanceof Sequence) {
            ((Sequence) this.right).addNewStatement(stmt);
        } else {
            this.right = new Sequence(this.right, stmt);
        }
    }

    @Override
    public int calculate() {
        return this.left.calculate() + this.right.calculate();
    }

    @Override
    public String getSymbol() {
      return this.left.getSymbol() + " " + this.right.getSymbol();
    }

    public String toString() {
        return "<SequenceNode>";
    }

    @Override
    public Stack<Expression> explode() {
        Stack<Expression> stack = new Stack<>();
        stack.push(this.right);
        stack.push(this.left);

        return stack;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
    }
}
