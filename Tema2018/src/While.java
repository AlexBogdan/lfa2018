import java.util.Stack;

public class While extends Statement {
    private BExp cond;
    private Block body;

    public While(BExp cond, Block body) {
        this.cond = cond;
        this.body = body;
    }

    public int calculate() {
        int res = 0;
        while (this.cond.calculate() == 1) {
            res = this.body.calculate();
        }
        if (this.next_stmt != null) {
            res = this.next_stmt.calculate();
        }
        return res;
    }

    public String toString() {
        return "<WhileNode> " + "while";
    }

    @Override
    public String getSymbol() {
        String res = "while" + this.cond.getSymbol() + this.body.getSymbol();
        if (this.next_stmt != null) {
            res += this.next_stmt.getSymbol();
        }
        return res;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.cond.accept(visitor);
        this.body.accept(visitor);
        visitor.done();
        if (this.next_stmt != null) {
            this.next_stmt.accept(visitor);
            visitor.done();
        }
    }
}
