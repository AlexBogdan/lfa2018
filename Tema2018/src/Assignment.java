import java.util.Stack;

public class Assignment extends Statement {
    private Variable left;
    private AExp right;

    public Assignment(Variable left, AExp right) {
        this.left = left;
        this.right = right;
    }

    private void assignVariable() {
        Variables.setVarValue(this.left.getVarName(), this.right.calculate());
    }

    public int calculate() {
        this.assignVariable();
        if (this.next_stmt != null) {
          return this.next_stmt.calculate();
        }
        return 0;
    }

    public String getSymbol() {
        String res = this.left.getSymbol() + "=" + this.right.getSymbol() + ";";
        if (this.next_stmt != null) {
            res += this.next_stmt.getSymbol();
        }
        return res;
    }

    public String toString() {
        return "<AssignmentNode> " + "=";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<Expression>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
        if (this.next_stmt != null) {
            this.next_stmt.accept(visitor);
            visitor.done();
        }
    }
}
