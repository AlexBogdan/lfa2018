import java.util.HashMap;

final class Variables {

    private static HashMap<String, Integer> map = new HashMap<>();

    static void setVarValue(String varName, int varValue) {
        map.put(varName, varValue);
    }

    static int getVarValue(String varName) {
        return map.get(varName);
    }

    public static HashMap<String, Integer> getVars() {
        return map;
    }
}
