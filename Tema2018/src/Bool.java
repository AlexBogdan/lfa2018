import java.util.Stack;

public class Bool extends BExp {
    private int value;

    public Bool(int value) {
        this.value = value;
    }

    public Bool(Boolean value) {
        if (value) {
            this.value = 1;
        } else {
            this.value = 0;
        }
    }

    public int calculate() {
        return this.value;
    }

    public String getSymbol() {
        return (this.value == 1) ? "true" : "false";
    }

    public String toString() {
        return "<BoolNode> " + this.getSymbol();
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        visitor.done();
    }
}
