import java.util.Stack;

public class Symbol implements Expression {
    String symbol;

    public Symbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public int calculate() {
        return -1;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    public String toString() {
        return symbol;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        visitor.done();
    }
}
