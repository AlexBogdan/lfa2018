import java.util.Stack;

public class Int extends AExp {

    private int value;

    public Int(int value) {
        this.value = value;
    }

    public int calculate() {
        return this.value;
    }

    public String getSymbol() {
      return new Integer(this.value).toString();
    }

    public String toString() {
        String Int = new Integer(this.value).toString();
        return "<IntNode> " + Int;
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        visitor.done();
    }
}
