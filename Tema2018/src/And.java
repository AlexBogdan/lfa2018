import java.util.Stack;

public class And extends BExp implements Visitable{
    private BExp left;
    private BExp right;

    public And(BExp left, BExp right) {
        this.left = left;
        this.right = right;
    }

    public int calculate() {
        return this.left.calculate() & this.right.calculate();
    }

    public String getSymbol() {
      return this.left.getSymbol() + "&&" + this.right.getSymbol();
    }

    @Override
    public Stack<Expression> explode() {
        Stack<Expression> stack = new Stack<Expression>();
        stack.push(this.right);
        stack.push(new Symbol("&&"));
        stack.push(this.left);

        return stack;
    }

    public String toString() {
        return "<AndNode> " + "&&";
    }

    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
    }
}
