import java.util.Stack;

public class Plus extends AExp {
    private AExp left;
    private AExp right;

    public Plus(AExp left, AExp right) {
        this.left = left;
        this.right = right;
    }

    public AExp getLeft() {
        return this.left;
    }

    public AExp getRight() {
        return  this.right;
    }

    @Override
    public int calculate() {
        return this.left.calculate() + this.right.calculate();
    }

    @Override
    public String getSymbol() {
      return this.left.getSymbol() + "+" + this.right.getSymbol();
    }

    public String toString() {
        return "<PlusNode> " + "+";
    }

    @Override
    public Stack<Expression> explode() {
        Stack<Expression> stack = new Stack<>();
        stack.push(this.right);
        stack.push(new Symbol("+"));
        stack.push(this.left);

        return stack;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.left.accept(visitor);
        this.right.accept(visitor);
        visitor.done();
    }
}
