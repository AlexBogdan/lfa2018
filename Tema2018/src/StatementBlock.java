import java.util.Stack;

public class StatementBlock extends Block {
    private Statement s;

    public StatementBlock(Statement s) {
        this.s = s;
    }

    public int calculate() {
        int res = s.calculate();
        if (this.next_stmt != null) {
            res = this.next_stmt.calculate();
        }
        return res;
    }

    public String getSymbol() {
        String res = "{" + s.getSymbol() + "}";
        if (this.next_stmt != null) {
            res += this.next_stmt.getSymbol();
        }
        return res;
    }

    public String toString() {
        return "<BlockNode> " + "{}";
    }

    @Override
    public Stack<Expression> explode() {
        return new Stack<>();
    }

    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        this.s.accept(visitor);
        visitor.done();
    }
}
