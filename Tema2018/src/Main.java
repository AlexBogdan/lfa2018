import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Iterator;

public class Main {

	private static void printAST(ProgramRoot root) {
		// Printeaza AST-ul
		BufferedWriter out = null;
		try {
				FileWriter fstream = new FileWriter("arbore", false); //true tells to append data.
				out = new BufferedWriter(fstream);

				ASTVisitor visitor = new ASTVisitor(out);
				root.accept(visitor);
		}
		catch (IOException e) {
				System.err.println("Error: " + e.getMessage());
		}
		finally {
			if(out != null) {
				try {
					out.close();
				}
				catch (IOException e){

				}
			}
		}
	}

	private static void printVariables() {
		// Printeaza variabilele din program si valorile acestora
		BufferedWriter out = null;
		try {
				FileWriter fstream = new FileWriter("output", false); //true tells to append data.
				out = new BufferedWriter(fstream);
				Iterator it = Variables.getVars().entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry var = (Map.Entry)it.next();
					out.write(var.getKey() + "=" + var.getValue() + "\n");
					it.remove();
				}
		}
		catch (IOException e) {
				System.err.println("Error: " + e.getMessage());
		}
		finally {
			if(out != null) {
				try {
					out.close();
				}
				catch (IOException e){

				}
			}
		}
	}


	public static void main(String[] args) {

		BufferedReader br;
		try {
			System.out.println("Working Directory = " + System.getProperty("user.dir"));

			br = new BufferedReader(new FileReader("input"));
			Flexer scanner = new Flexer(br);
			// add something to variable map (multiple instructions are not yet supported)
			scanner.yylex();

			// Printeaza variabilele din program si valorile acestora
			printAST(scanner.root);

			// executa toate comenzile din ProgramRoot
			scanner.root.calculate();

			// Printeaza variabilele din program si valorile acestora
			printVariables();
		}
		catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
}
